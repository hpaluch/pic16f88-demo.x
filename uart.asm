; uart.asm - UART handling code for PIC16F88

	LIST      P=16F88           ; list directive to define processor
	INCLUDE <p16F88.inc>        ; processor specific variable definitions

; all uart functions are prefixed with UA_

UA_CODE CODE
; UART init
cUA_INIT:
    GLOBAL cUA_INIT
; init TX part of UART, 9600Bd, 8-bit, 1-start, 1-stop, no parity
; see DS30487D-page 103

    ; 1. set SPBRG (baud rate)
    BANKSEL SPBRG
    MOVLW   .25  ; 9600 Bd for fOSC=4MHz and BRGH=1
    MOVWF   SPBRG

    ; 2.set TXSTA - SYNC must be cleared, BRGH=1 for better precision
    ;   CSRC = 0  Don't care (Async)
    ;   TX9  = 0  8-bit transmittion
    ;   TXEN = 0  Transmit NOT YET enabled
    ;   SYNC = 0  Async (UART) mode
    ;   BRGH = 1  High Baud Rate enabled
    ;   TRMT = 0 (Read-only register)
    ;   TX9D = 0 9-bit value - unused
    BANKSEL TXSTA
    MOVLW   (1<<BRGH)
    MOVWF   TXSTA

    ; 3. required TRISB setup for UART. See DS30487D-page 97
    BSF TRISB,5
    BSF TRISB,2 

    ; 3. must enable SPEN
    BANKSEL RCSTA
    MOVLW   (1<<SPEN)
    MOVWF   RCSTA

    ; 4 enable transmit - TXEN=1
    BANKSEL TXSTA
    MOVLW   (1<<BRGH)|(1<<TXEN)
    MOVWF   TXSTA

; init RX part of UART, 9600Bd, 8-bit, 1-start, 1-stop, no parity
; see DS30487D-page 105
    ; 1. set Baud Rate in SPBRG - already done in TX part
    ; 2. clear SYNC - done
    ; 3. enable SPEN - done
    ; 4. clear RX9D - done
    ; 5. enable RX reception - setting CREN
    BANKSEL RCSTA
    BSF     RCSTA,CREN

    BANKSEL TXREG   ; back to BANK0 (main program expect this)
    RETURN

; handle frame error - UNTESTED
gUA_RX_FERR:
    MOVFW   RCREG   ; not sure whether this is needed?
    MOVLW   '!'     ; TX error
    CALL    cUA_TX_W
    MOVLW   'F'     ; F-raming error
    CALL    cUA_TX_W
    GOTO    gUA_RX_ERR_RECOV   ; recovery after error
; handle overrun error - UNTESTED
gUA_RX_OERR:
    MOVLW   '!'     ; TX error
    CALL    cUA_TX_W
    MOVLW   'O'     ; O-verrun error
    CALL    cUA_TX_W
    ; must reset receiver...
gUA_RX_ERR_RECOV:
    BCF     RCSTA,CREN
    BSF     RCSTA,CREN
    ; try again
    GOTO    cUA_RX_W

; read data from PC via UART, RS232 - return in W register
cUA_RX_W:
    GLOBAL cUA_RX_W

    BANKSEL PIR1
gUA_RX1
    BTFSS   PIR1,RCIF   ; wait as long as RXIF is 0 (=no data available)
    GOTO    gUA_RX1
    BTFSC   RCSTA,FERR
    GOTO    gUA_RX_FERR
    BTFSC   RCSTA,OERR
    GOTO    gUA_RX_OERR
; no error just read data
    MOVFW   RCREG
    RETURN

; send content of W register to PC via UART, RS232
cUA_TX_W:
    GLOBAL cUA_TX_W

    BANKSEL PIR1
gUA_TX1
    BTFSS   PIR1,TXIF   ; wait as long as TXIF is 0 (=TXREG full)
    GOTO gUA_TX1
    MOVWF   TXREG       ; transmit W from PIC to PC via UART
    RETURN

    END
