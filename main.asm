; PIC16F88 - my first demo project with this MCU
; build on MPLAB X IDE v2.35
;
; Current function:
; 1. should output FOSC/4: 4 MHz / 4 = 1MHz on PIN 15 RA6/OSC2/CLKO
; 2. output 7.6 Hz on RA0 PIN 17 - connected to Speaker
; 3. RA1 PIN 18 - LEDs controlled by commands on RS232 port

	LIST      P=16F88           ; list directive to define processor
	#include <p16F88.inc>        ; processor specific variable definitions
    INCLUDE "uart.inc" ; our UART (USB serial) handling code

; NOTE:
; Debug is ON for __DEBUG, Off for Run
; Brown-out RESET ON
; /MCLR ON
; INTRC CLOCK OUT
; PWRTE Off for Debug, On for Run
#ifdef __DEBUG
    messg "DEBUG Mode"
	__CONFIG    _CONFIG1, _CP_OFF & _CCP1_RB0 & _DEBUG_ON & _WRT_PROTECT_OFF & _CPD_OFF & _LVP_OFF & _BOREN_ON & _MCLR_ON & _PWRTE_OFF & _WDT_OFF & _INTRC_CLKOUT
#else
    messg "RUN Mode"
	__CONFIG    _CONFIG1, _CP_OFF & _CCP1_RB0 & _DEBUG_OFF & _WRT_PROTECT_OFF & _CPD_OFF & _LVP_OFF & _BOREN_ON & _MCLR_ON & _PWRTE_ON & _WDT_OFF & _INTRC_CLKOUT
#endif
	__CONFIG    _CONFIG2, _IESO_OFF & _FCMEN_OFF

; My I/O ports
iSPKR_BIT   EQU RA0
iLED_BIT    EQU RA1

; *Shared* Uninitialized Data Section
INT_VAR		UDATA_SHR	0x71
w_temp		RES     1		; variable used for context saving
status_temp	RES     1		; variable used for context saving
pclath_temp	RES     1		; variable used for context saving

; Bank 0 RAM registers - 
TEMP_VAR    UDATA   0x20     ; explicit address specified is not required
rT0_CNT     RES     1        ; counter on Timer0 overflow (from Interrupt)
rCMD        RES     1


;**********************************************************************
RESET_VECTOR	CODE	0x000   ; processor reset vector
		goto    main            ; go to beginning of program

INT_VECTOR	CODE	0x004		; interrupt vector location
		movwf   w_temp          ; save off current W register contents
		movf	STATUS,w        ; move status register into W register
		movwf	status_temp     ; save off contents of STATUS register
		movf	PCLATH,W        ; move pclath register into W register
		movwf	pclath_temp	  ; save off contents of PCLATH register
ISR_START
        ; must be careful - interrupt may occur at any time
        PAGESEL ISR_START
        BTFSS INTCON,TMR0IF
        GOTO  gNoT0Overflow
; Timer Overflow is on:
; 256 * 256 =  65 536 us = 65.536 ms
        BCF     INTCON,TMR0IF ; Ack Timer0 Overflow
        BANKSEL rT0_CNT
        INCF    rT0_CNT,f
       ; BIT 0 has period 2 * 65.536 ms = 131.072 ms => 7.6 Hz
       ; copy bit-0 of rT0_CNT to iSPKR_BIT on PORTA, keep other bits unchanged
       BTFSS    rT0_CNT,0
       BCF      PORTA,iSPKR_BIT
       BTFSC    rT0_CNT,0
       BSF      PORTA,iSPKR_BIT

gNoT0Overflow
; ISR END
		movf	pclath_temp,W	  ; retrieve copy of PCLATH register
		movwf	PCLATH		  ; restore pre-isr PCLATH register contents
		movf    status_temp,w     ; retrieve copy of STATUS register
		movwf	STATUS            ; restore pre-isr STATUS register contents
		swapf   w_temp,f	  ;
		swapf   w_temp,w          ; restore pre-isr W register contents
		retfie                    ; return from interrupt

main:
; main code goes here
        MOVLW   ( B'110' << IRCF0 )     ; set OSC to 4MHz (default was 32.768kHz)
        BANKSEL OSCCON
        MOVWF   OSCCON

; PORTA init - from Example 5-1 of 16F88 Data Sheet
        BANKSEL PORTA   ; select bank of PORTA
        CLRF PORTA      ; Initialize PORTA by clearing output data latches
        BANKSEL ANSEL   ; Select Bank of ANSEL
        MOVLW 0x00      ; Configure all pins
        MOVWF ANSEL     ; as digital inputs
        MOVLW ~((1 << iSPKR_BIT)|(1<< iLED_BIT)) ; Value used to initialize data direction
        MOVWF TRISA  ; Set RA0,RA1 to output (Speaker and LEDs)

; prepare Timer0 - prescale as much as possible
        CLRWDT  ; required even when WDT disabled
        BANKSEL OPTION_REG
        ; /RBPU disable PORTB Pull-Ups
        ; T0CS  0 => Timer0 source is CLCKO
        ; PSA   0 => Prescaler to Timer0
        ; PS<2:0> 0x7 => Prescaler set to 1:256
        MOVLW   (1<<NOT_RBPU)|0x7
        MOVWF   OPTION_REG

        ; initialize UART
        LCALL    cUA_INIT
        PAGESEL gMAIN1

        ; Reset Timer0 (just to be sure)
        BANKSEL TMR0
        CLRF    TMR0
        BANKSEL rT0_CNT
        CLRF    rT0_CNT
        ; enable Timer0 overflow interrupt
        BSF INTCON,TMR0IE
        ; enable interrupts globally
        BSF INTCON,GIE

        ; main loop
gMAIN1
        ; read command from RS232
        LCALL    cUA_RX_W

        BANKSEL rCMD
        MOVWF   rCMD

; currently we accept only '0' or '1' as command
        SUBLW   '1'        ; '1' - W => for '1' => 0, for '0' => 1
        PAGESEL gCMD_ERROR
        BTFSS   STATUS,C    ; Carry is inverted for SUBLW - see DS31029A-page 29-39
        GOTO    gCMD_ERROR
        ANDLW   ~1         ; if other bits are set => invalid value
        BTFSS   STATUS,Z
        GOTO    gCMD_ERROR

        ; now we just copy lowest bit from rCMD to iLED bit
        BTFSS   rCMD,0
        BCF     PORTA,iLED_BIT
        BTFSC   rCMD,0
        BSF     PORTA,iLED_BIT

        ; acknowledge success
        MOVLW   '='         ; = OK
        LCALL    cUA_TX_W
        MOVFW   rCMD        ; copy command data (for verification)
        LCALL    cUA_TX_W

        ; main loop again
        LGOTO    gMAIN1

gCMD_ERROR:
        MOVLW   '!'
        LCALL    cUA_TX_W
        MOVLW   'C'         ; "!C" = Command error
        LCALL    cUA_TX_W

        LGOTO    gMAIN1

		END

