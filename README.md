# Demo project with PIC16F88

Demo project with [PIC16F88] - LEDs D1, D2 controlled by commands
send via serial port.

Project status:

1. Outputs FOSC/4 on pin 15 RA6/OSC2/CLKO.
1. output 7.6/4 on RA0 PIN 17 - connected to Speaker
1. LEDs on RA1 controlled via Serial commands `0` or `1`


Supported commands on serial port:

* `0` - sends logical 0 to RA1 - red LED D1 is on, green LED D2 is off
* `1` - sends logical 1 to RA1 - red LED D1 is off, green LED D2 is on

Answers from PIC:

* `=0` or `=1` - command processed successfully - the number is value
  from command (`0` or `1`)
* `!C` - invalid command - entered anything other than `0` or `1`
* `!F` - framing error - invalid data received from serial port
* `!O` - overrun error - some data lost from serial port

Example communication:
```
Send: 0
Received: =0
Send: 1
Received: =1
Send: a
Received: !C
```


![PIC16F88 demo on PICDEM board](ExpressPCB/pic16f88-demo.png)

Why [PIC16F88]?

1. It contains In Circuit Debugger (ICD) - you can debug it with
   bare PicKit 3 without need for adapter headers

1. Is included with [PICDEM Demo board][DM163045]    


> NOTES: 
>
> Measured frequency on `RA6/OSC2/CLKO` PIN 15 should be 4 Mhz / 4 = 1 MHz
>
> Mine has 1.002 MHz, which is very good (+ 0.2%)
>


> WARNING!
>
> In Debug mode there can be large delay (even 30s) before program startup.

# Requirements

* [DM163045 - PICDEM Lab Development Kit][DM163045], this kit includes also
  PicKit 3 Programmer/Debugger and PIC MCUs samples
* [USB Console Cable #954] to transfer serial data from PIC to PC  
* [PIC16F88][PIC16F88] - included with [DM163045 PICDEM kit][DM163045].
  Please see [PIC16F88 Data Sheet] for more information on this MCU
* [MPLAB X IDE v2.35] - the oldest version that checks calibration value
  and allow overwrite of calibration and still fast and works under XP
  (unlike current version 4.x)

Architecture overview:

```
+-----------+      +-----------+
|MPLAB X IDE|----->|PicKit3    |-----------+
+-----------+      +-----------+           | ICSP2 J12
                                           V
+-----------+      +-----------+      +------------+
|PC Putty   |      |USB Console|      |PICDEM board|
|Serial mode|----->|Cable #954 |----->|PIC16F88    |
+-----------+      +-----------+      +------------+
```


# Fixing driver for "USB Console Cable #954"

If you have poor luck (like me) you can have troubles with
your cable and recent Windows drivers.

My cable was labeled on e-shop
as `USB to TTL Serial Cable - Debug /Console Cable for Raspberry Pi`
Model `FFR-JMP` ordered from ModMyPi on 2013.
It is visible on archive page https://web.archive.org/web/20140425050649/https://www.modmypi.com/raspberry-pi-accessories
(right top corner). The bag containing cable is labeled `USB Console Cable #954`.

> Disclaimer: I have no clue which series are affected. I don't know
> whether current version available
> from [USB Console Cable #954]
> exhibits this problem or not.

The problem:

> Official driver does not work with this "flavor" of Prolific
> chip - there is an error `The device cannot start (Code 10)`
> in Windows Device Manager
> Fortunately fix available on:
> - http://www.totalcardiagnostics.com/support/Knowledgebase/Article/View/92/20/prolific-usb-to-serial-fix-official-solution-to-code-10-error

## USB Driver Fix

If you have problematic `Prolific chip` then:

1. Download older driver from
   http://www.totalcardiagnostics.com/support/Knowledgebase/Article/View/92/20/prolific-usb-to-serial-fix-official-solution-to-code-10-error
   in case of XP I used following 32-bit driver from above page:
   http://www.totalcardiagnostics.com/files/PL-2303_Driver_Installer.exe

1. install downloaded driver

## Common USB Serial cable setup

1. connect "USB Console Cable #954" to PICDEM board as shown on schematic 

1. connect "USB Console Cable #954" to PC

1. Power-up PICDEM board

1. Download and install Putty from:
   https://the.earth.li/~sgtatham/putty/0.70/w32/putty-0.70-installer.msi

1. Get name of your USB Serial COM port:
    - open "Device Manager" - for example run `devmgmt.msc` 
    - expand "Ports (COM & LPT)"
    - remember your `x` value from:
      `Prolific USB-To-Serial Comm port (COMx)` 
    
1. Configure Putty connection:
    - type: `Serial`
    - "Serial line:" `COMx` (see above how to get `x` value)
    - expand Connection -> Serial and ensure following settigns:
        - "Serial line to connect to:" `COMx` (see above hot to get `x` value)                                           
        - "Speed (baud):" `9600`
        - "Data bits:" `8`
        - "Stop bits:" `1`
        - "Parity:" `None`
        - "Flow control:" `None`

1. Open this serial connection in putty

1. Enter `0` or `1` as command. LEDs should light accordingly.
   and reply `=0` or `=1` should be received immediately.

# Relocation caution

This project now uses two `*.asm` relocatable modules (key addresses are
resolved in MP-LINK stage - they are unknown in MP-ASM stage).

When building in Debug mode the `uart.asm` (section `UA_CODE`) 
 **is in different page than main code!**
as seen in `dist\default\debug\pic16f88-demo.X.debug.map`: 
```
                                 Section Info
                  Section       Type    Address   Location Size(Bytes)
                ---------  ---------  ---------  ---------  ---------
             RESET_VECTOR       code   0x000000    program   0x000002
               INT_VECTOR       code   0x000004    program   0x000096
                   .cinit    romdata   0x000800    program   0x000004
                  UA_CODE       code   0x000802    program   0x000032
                    dbgP0       code   0x000f00    program   0x000200
```

When build for Production it is packed together with `main.asm` as seen
in `dist\default\production\pic16f88-demo.X.production.map`:
```
                                 Section Info
                  Section       Type    Address   Location Size(Bytes)
                ---------  ---------  ---------  ---------  ---------
             RESET_VECTOR       code   0x000000    program   0x000002
                   .cinit    romdata   0x000001    program   0x000004
               INT_VECTOR       code   0x000004    program   0x000096
                  UA_CODE       code   0x00004f    program   0x000032
```

It is therefore **necessary** to user proper `PAGESEL` before calling
UART routines from main code - to make them work properly in Debug mode!

> REMAINDER: Always check generated `*.map` files.

# Resources

* [DM163045 - PICDEM Lab Development Kit][DM163045]
* [PIC16F88]
* [PIC16F88 Data Sheet]
* [PIC ASM OBJ Templates] - relocatable ASM templates - file
  `f88tempo.asm` was used as initial template for [main.asm]
* [MPLAB X IDE v2.35]

[MPLAB X IDE v2.35]: http://www.microchip.com/development-tools/pic-and-dspic-downloads-archive "MPLAB Archive Download"
[DM163045]: http://www.microchip.com/Developmenttools/ProductDetails/DM163045 "PICDEM Lab Development Kit"
[PIC16F88]: https://www.microchip.com/wwwproducts/en/PIC16F88 "PIC16F88 Overview"
[PIC16F88 Data Sheet]: http://ww1.microchip.com/downloads/en/DeviceDoc/30487D.pdf "PIC16F88 Data Sheet"
[USB Console Cable #954]: https://www.modmypi.com/raspberry-pi/communication-1068/serial-1075/usb-to-ttl-serial-cable-debug--console-cable-for-raspberry-pi "USB Console Cable #954" 
[PIC ASM OBJ Templates]: http://ww1.microchip.com/downloads/en/DeviceDoc/obj.zip
[main.asm]: https://bitbucket.org/hpaluch/pic16f88-demo.x/src/master/main.asm?fileviewer=file-view-default
